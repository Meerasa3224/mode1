<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Registration Page</title>
<script type="text/javascript">

 function validatePassword(){
	var userpass=document.getElementById("userPassword").value;
	var confirmpass=document.getElementById("userConformPassword").value;
	
	if(userpass!=confirmpass){
		alert("Password do not match");	
	}
	 else{
		document.getElementById("regForm").submit();
	} 
	
	
	
}  
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row1" style="width:100%;height:40px" >
		 <div class="col-md-12" style="background-color:black;height:200%">
		 	<h4 style="float:left;color:white;font-size:20px;padding-top-left:20px;padding-top-left:20.0px">PetShop</h4>
			<a href="login" style="float:right;font-size:20px;color:white;padding-right:20px;padding-top:6.5px">Login</a>
		</div>
	
	</div>


</div>
<div class="container-fluid">
	<div class="row2" style="width:100%;height:700px">
		<div class="col-md-12" style="background-color:darkcyan;height:150%;margin-top:50px">
             <h2 align="center">Register</h2>
			<c:url var="userAction" value="/authenticateUser"/>
			<form:form action="${userAction}" id="regForm" onsubmit="return validatePassword()" method="post" modelAttribute="userRegisterForm" >
			<table align="center">
			<tr>
				<td>Name</td>
				<td><form:input type="text" path="userName" size="50"/>
				         <form:errors path="userName"></form:errors></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><form:input type="password" path="userPassword" id="userPassword" size="50"/>
				         <form:errors path="userPassword"></form:errors></td>
			</tr>
			<tr>
				<td>ConformPassword</td>
				<td><form:input type="password" path="userConformPassword"  id="userConformPassword" size="50"/>
				         <form:errors path="userConformPassword"></form:errors></td>
			</tr>
			<tr>
				<td><input type="submit"  value="Register" ></td>
			</tr>
			
			</table>
			</form:form>
		</div>
	</div>
</div>

</body>
</html>