<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Pet Page</title>
<style>
 a{
 text-decoration:none;
 }

</style>
</head>
<body>
<div class="col-md-12" style="background-color:#051E3E; height:100%">
<div class="container-fluid">
	<div class="row1" style="width:100%;height:40px" >
		 <div class="col-md-12" style="background-color:black;height:100%">
		 	<h4 style="float:left;color:white;font-size:20px">PetShop</h4>
		 	<a href="homePage" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">Home</a>
		 	<a href="MyPet" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">MyPets</a>
		 	<a href="AddPet" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">AddPets</a>
			<a href="register" style="float:right;font-size:20px;color:white;padding-right:20px;padding-top:6.5px">Logout</a>
		</div>
	
	</div>
</div>
<div class="container-fluid">
	<div class="row2" style="width:50%;height:700px;margin-left:300px;margin-top:70px">
		<div class="row3" style="background-color:grey;width:100%">
		<h3>Pet Information</h3>
		</div>
		<div class="row4" style="background-color:#7BC043;width:100%">
		<form:form action="/addPetInto" method="post" modelAttribute="petAddForm">
			<table align="center">
				<tr>
					<td>PetId:</td>
					<td ><form:input type="text" path="petId" size="50" />
					          <form:errors path="petId"></form:errors></td>
				</tr>
				<tr>
					<td>PetName:</td>
					<td><form:input type="text" path="petName" size="50"/>
					         <form:errors path="petName"></form:errors></td>
				</tr>
				<tr>
					<td>PetAge:</td>
					<td><form:input type="text" path="petAge" size="50"/>
					         <form:errors path="petAge"></form:errors></td>
				</tr>
				 <tr>
					<td>PetPlace:</td>
					<td><form:input type="text" path="petPlace" size="50"/>					     
					        <form:errors path="petPlace"></form:errors></td>
				</tr> 
				<tr>
					<td><input type="submit" value="AddPet" /></td>
					<td><input type="reset" value="Cancel"/></td>
				</tr>
			
			</table>
		</form:form>
		</div>
		</div>
	</div>
</div>
		

</body>
</html>