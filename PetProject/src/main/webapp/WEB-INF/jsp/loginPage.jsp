<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
</head>
<body>

	<div class="container-fluid">

		<div class="row1" style="width: 100%; height: 40px">
			<div class="col-md-12" style="background-color: black; height: 200%">
				<h4 style="float: left; color: white; font-size: 20px; padding-left:20px">PetShop</h4>
				<a href="register" style="float: right; font-size: 20px; color: white; padding-right: 20px; padding-top: 6.5px">SignUp</a>
			</div>

		</div>


	</div>
	<div class="container-fluid">
		<div class="row2" style="width: 100%; height: 700px">
			<div class="col-md-12" style="background-color:darkcyan; height:100%;margin-top:50px">

				<h2 align="center">
					Login:<br/>${message}</h2>

				<c:url var="Action" value="/homePage" />
				<form:form action="${Action }" method="post"
					modelAttribute="loginForm">
					<table align="center">
						<tr>
							<td>Name:</td>
							<td> <form:input type="text" path="userName" size="50" /> 
							      <form:errors path="userName"></form:errors></td>
						</tr>
						<tr>
							<td>Password:</td>
							<td> <form:input type="password" path="userPassword" size="50" />
							     <form:errors path="userPassword"></form:errors></td>
						</tr>

						<tr>
							<td>
							<input type="submit" value="login"/></td>
						</tr>
					</table>
				</form:form>
			</div>
		</div>
	</div>    
</body>
</html>