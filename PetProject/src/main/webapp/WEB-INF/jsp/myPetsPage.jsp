<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My pets Page</title>
<style>
 a{
 text-decoration:none;
 }

</style>
</head>
<body>
<div class="col-md-12" style="background-color:darkcyan; height:100%">
<div class="container-fluid">
	<div class="row1" style="width:100%;height:40px" >
		 <div class="col-md-12" style="background-color:black;height:100%">
		 	<h4 style="float:left;color:white;font-size:20px">PetShop</h4>
		 	<a href="homePage" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">Home</a>
		 	<a href="MyPet" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">MyPets</a>
		 	<a href="AddPet" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">AddPets</a>
			<a href="registerationPage" style="float:right;font-size:20px;color:white;padding-right:20px;padding-top:6.5px">Logout</a>
		</div>
	
	</div>

</div>
<div class="container-fluid">
	<div class="row2" align="center" style="width:50%;height:700px;margin-left:300px;margin-top:140px">
		<div class="row3" align="center" style="background-color:white;width:100%">
	<div class="row2">
		<div class="col-md-12" >
		  <table border="1"style="width:100%">
			<tr align="center">
				<th>PetId</th>
				<th>PetName</th>
				<th>PetAge</th>
				<th>PetPlace</th>
				<th>Buy</th>	
			</tr>
			
			
			<tr>
				<td><%=session.getAttribute("buypet")%></td>
				<td><%=session.getAttribute("petName") %></td>
				<td><%=session.getAttribute("petAge") %></td>
				<td><%=session.getAttribute("petPlace") %></td>
			    <td><a href="<c:url value='/buy/${ pet.petid}'/>">Buy</a></td>		
			</tr>
		</table>
		</div>
	</div>
</div>
</div>
</body>
</html>