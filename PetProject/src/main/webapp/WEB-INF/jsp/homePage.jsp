<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home page</title>
<style>
 body {
    background:darkcyan;
}

</style>
<script>
	function myFunction(){
		document.getElementById("show").innerHTML="sold out";
	}
	
</script>
</head>
<body background="meerasa.jpg">
<div class="container-fluid">
	<div class="row1" style="width:100%;height:40px">	  
		 <div class="col-md-12" style="background-color:black;height:150%">
		 	<h4 style="float:left;color:white;font-size:20px;padding-top:6.5px">PetShop</h4>
		 	<a href="homePage" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">Home</a>
		 	<a href="MyPet" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">MyPets</a>
		 	<a href="addPet" style="float:left;font-size:20px;color:white;padding-left:20px;padding-top:6.5px">AddPets</a>
			<a href="register" style="float:right;font-size:20px;color:white;padding-right:20px;padding-top:6.5px">Logout</a>
		</div>
	
	</div>


</div>
<div class="container-fluid">
<div class="row2"style="background-color:white;margin-left:300px;margin-top:150px;width:500px">	
<div class="col-md-12">
<%-- <h1>Welcome<%=request.getParameter("username") %></h1>
 --%>		<table border="1" style="width:500px">
			<tr>
				<th>PetId</th>
				<th>PetName</th>
				<th>PetAge</th>
				<th>PetPlace</th>
				<th>Buy</th>
				
				
			</tr>
			
			<c:forEach items="${petList}" var="pet">
			<tr>
				<td>${pet.petId }</td>
				<td>${pet.petName}</td>
				<td>${pet.petAge}</td>
				<td>${pet.petPlace}</td>
				<td id="show"><a href="<c:url value='/buyPet/${pet.petId}'/>  ">Buy</a></td>
			</tr>

		</c:forEach>
		
		</table>
		</div>
		</div>
</div>
	
</body>
</html>