package com.casestudy.service;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.PetDao;
import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Service
@Transactional
public class PetServiceImpl implements PetService {
	@Autowired
	private PetDao petDao;

	@Override
	public Pet savePet(Pet pet) {
		
		 return petDao.savePet(pet);
	}

	@Override
	public List<Pet> getMyPets(User user) {
		return petDao.getMyPets(user);
	}

	@Override
	public Pet buyPet(int petId, User user) {
		return petDao.buyPet(petId, user);
	}

	@Override
	public List<Pet> getAllPets() {
		return petDao.getAllPets();
	}
}
