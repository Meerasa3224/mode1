package com.casestudy.service;

import java.util.List;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

public interface PetService {
	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getMyPets(User user);

	public abstract Pet buyPet(int petId, User user);

	public abstract List<Pet> getAllPets();
}
