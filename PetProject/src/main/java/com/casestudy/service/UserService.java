package com.casestudy.service;

import com.casestudy.model.User;

public interface UserService {
	public boolean register(User user);

	public User login(User user1);
}
