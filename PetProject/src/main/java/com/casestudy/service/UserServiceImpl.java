package com.casestudy.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.UserDao;
import com.casestudy.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	@Override
	public boolean register(User user) {

		return userDao.register(user);
	}

	@Override
	public User login(User user1) {
		return userDao.login(user1);
	}

}
