package com.casestudy.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="user1")
public class User {
	@Id
	@GenericGenerator(name="increment-gen",strategy="increment")
	private int userId;
	@NotEmpty
	private String userName;
	@NotEmpty
	private String userPassword;
	@Transient
	@NotEmpty
	private String userConformPassword;
	@OneToMany(mappedBy="user")
	private List<Pet> pet;

	public User() {
		super();

	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserConformPassword() {
		return userConformPassword;
	}

	public void setUserConformPassword(String userConformPassword) {
		this.userConformPassword = userConformPassword;
	}

	public List<Pet> getPet() {
		return pet;
	}

	public void setPet(List<Pet> pet) {
		this.pet = pet;
	}

	public User(int userId, String userName, String userPassword, String userConformPassword, List<Pet> pet) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPassword = userPassword;
		this.userConformPassword = userConformPassword;
		this.pet = pet;
	}

}