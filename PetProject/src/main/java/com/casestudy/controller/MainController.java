package com.casestudy.controller;

import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;
/**
 * 
 * @author Meerasa
 *
 */
@Controller
public class MainController {
	@Autowired
	private PetService petService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		User user = new User();
		modelAndView.addObject("userRegisterForm", user);
		return modelAndView;
	}

	@RequestMapping("/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("loginPage");
		User user = new User();
		modelAndView.addObject("loginForm", user);
		return modelAndView;
	}

	@RequestMapping(value = "/authenticateUser", method = RequestMethod.POST)
	public String authenticateUser(@Validated @ModelAttribute("userRegisterForm") User user, BindingResult result,
			Model map, HttpServletRequest req, RedirectAttributes redirect) {

		String response = "";
		if (result.hasErrors()) {
			System.out.println("Registration has errors");
			response = "registrationPage";
		} else {
			Random random = new Random();

			int userId = random.nextInt(99999) + 10000;
			user.setUserId(userId);

			if (userService.register(user) == true) {
				System.out.println("registration successful,please login");
				map.addAttribute("message", "Resgistration Successfull");
				map.addAttribute("loginForm", user);
				response = "loginPage";
			} else {
				map.addAttribute("message", "user already exist");
				response = "registrationPage";
			}
		}
		return response;
	}

	@RequestMapping("/register")
	public ModelAndView register() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		User user = new User();
		modelAndView.addObject("userRegisterForm", user);
		return modelAndView;
	}

	@RequestMapping(value = "/AddPet")
	public ModelAndView addPet() {
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		Pet pet = new Pet();
		modelAndView.addObject("petAddForm", pet);
		return modelAndView;
	}

	@RequestMapping(value = "/homePage", method = RequestMethod.POST)
	public String home(@Validated @ModelAttribute("userRegisterForm") User user, BindingResult result, Model map,
			HttpServletRequest req) {
		System.out.println("the username and passowrd");
		User checkuser = userService.login(user);
		String response = "";
		if (checkuser != null) {
			System.out.println("correct login credentials");
			HttpSession session = req.getSession();
			session.setAttribute("checkuser", checkuser);
			map.addAttribute("checkuser", session.getAttribute("checkuser"));
			response = "redirect:/homePage";
		} else {
			System.out.println("incorrect");
			map.addAttribute("message", "incorrect username or password");
			map.addAttribute("loginForm", user);
			response = "loginPage";
		}
		return response;
	}


	@RequestMapping(value = "/addPetInto", method = RequestMethod.POST)
	public String addPet(@Validated @ModelAttribute("petAddForm") Pet pet, BindingResult result, ModelMap map) {
		String response = "";
		if (result.hasErrors()) {
			response = "addPetPage";
		} else {
			Random random = new Random();
			int petId = random.nextInt(99999) + 10000;
			pet.setPetId(petId);

			petService.savePet(pet);

			response = "redirect:/homePage";
		}
		return response;

	}
	@RequestMapping(value = "/homePage")
	public String home(ModelMap map) {
		List<Pet> list = petService.getAllPets();
		map.addAttribute("petList", list);
		return "homePage";
	}

	@RequestMapping(value = "/MyPet")
	public String myPet(ModelMap map) {
		return "myPetsPage";
	}
}
