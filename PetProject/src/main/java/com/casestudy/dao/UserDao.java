package com.casestudy.dao;

import com.casestudy.model.User;

public interface UserDao {
	public boolean register(User user);

	public User login(User user1);
}
