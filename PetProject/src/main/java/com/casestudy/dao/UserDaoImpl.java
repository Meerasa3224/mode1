package com.casestudy.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean register(User user) {
		int check = (int) sessionFactory.getCurrentSession().save(user);
		if (check != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public User login(User user1) {
		System.out.println("In check login");
		Session session = sessionFactory.getCurrentSession();
		String query = "from User u where u.userName=:userName and u.userPassword=:userPassword";
		User user = (User) session.createQuery(query).setParameter("userName", user1.getUserName())
				.setParameter("userPassword", user1.getUserPassword()).uniqueResult();
		if (user != null) {
			System.out.println(" userName" + user1.getUserName());
			return (User) user;
		} else {
			System.out.println("wrong");
			return null;
		}

	}

}
