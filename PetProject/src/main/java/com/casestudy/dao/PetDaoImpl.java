package com.casestudy.dao;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.transaction.Transactional;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class PetDaoImpl implements PetDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Pet savePet(Pet pet) {
		/*return (Pet) sessionFactory.getCurrentSession().save(pet);*/
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pet);
		return pet;
	}
	

	@Override
	public List<Pet> getMyPets(User user) {
		/*Session session = sessionFactory.getCurrentSession();
		String sql = "from Pet p where p.user.Id=:uId";
		List<Pet> pet = session.createQuery(sql).setParameter("uId", user.getUserId()).list();
		return pet;*/
		Session session = this.sessionFactory.getCurrentSession();
		List<Pet> petList = session.createQuery("from Pet").list();
		for (Pet pet : petList) {
			petList.add(pet);
		}
		return petList;
	}

	@Override
	public Pet buyPet(int petId, User user) {
		Session session = sessionFactory.getCurrentSession();
		Pet pet = (Pet) session.get(Pet.class, petId);
		if (pet != null) {
			pet.setUser(user);
			session.save(pet);
			Pet updated_pet = (Pet) session.get(Pet.class, petId);
			return updated_pet;
		}

		return pet;
	}

	@Override
	public List<Pet> getAllPets() {
		Session session = sessionFactory.getCurrentSession();
		String sql = "from Pet p ";
		List<Pet> availPets = session.createQuery(sql).list();
		return availPets;
	
		
	}

}
