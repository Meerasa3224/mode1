package com.meerasa.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.meerasa.bean.Address;
import com.meerasa.bean.Department;
import com.meerasa.bean.Employee;

public class Main {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		Department department = (Department) applicationContext.getBean("depBean");
		System.out.println("Department Details");
		System.out.println();
		System.out.println("Department Number:" + department.getDeptNo());
		System.out.println("Department Name:" + department.getDeptName());
		System.out.println(".............................................");
		List<Employee> collection = department.getEmployees();
		for (Employee employee : collection) {
			System.out.println("Employee Details");
			System.out.println(".........................");
			System.out.println("Employee Id:" + employee.getEmpId());
			System.out.println("Employee Name:" + employee.getEmpName());
			System.out.println("Employee Salary:" + employee.getEmpSal());
			System.out.println("..........................................");

			List<Address> collectionAddress = employee.getAddresses();
			for (Address address : collectionAddress) {
				System.out.println("Employee Address Details");
				System.out.println("...................................");
				System.out.println("Employee Plot Number:" + address.getDoorNo());
				System.out.println("Employee Street:" + address.getStreet());
				System.out.println("Employee City:" + address.getCity());
				System.out.println("Employee State:" + address.getState());
			}
		}

	}

}
