package com.meerasa.springBoot.service;

import com.meerasa.springBoot.model.Student;

public interface StudentService {
	public abstract String addStudent(Student std);

	public abstract Student searchStudent(String sid);

	public abstract String updateStudent(Student std);

	public abstract String deleteStudent(String sid);
}
