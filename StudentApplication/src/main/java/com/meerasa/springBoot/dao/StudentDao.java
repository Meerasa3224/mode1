package com.meerasa.springBoot.dao;

import com.meerasa.springBoot.model.Student;

public interface StudentDao {
	    public abstract String add(Student std);
	    public abstract Student search(String sid);
	    public abstract String update(Student std);
	    public abstract String delete(String sid);
	}
	 
