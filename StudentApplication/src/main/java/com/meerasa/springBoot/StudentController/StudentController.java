package com.meerasa.springBoot.StudentController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meerasa.springBoot.model.Student;
import com.meerasa.springBoot.service.StudentService;

@Controller
public class StudentController {
	@Autowired
	StudentService studentService;

	@RequestMapping("/welcomepage")
	public String showWelcomePage() {
		return "welcome";
	}

	@RequestMapping("/addform")
	public String showAddPage() {
		return "addform";
	}

	@RequestMapping("/searchform")
	public String showSearchPage() {
		return "searchform";
	}

	@RequestMapping("/updateform")
	public String showUpdatePage() {
		return "updateform";
	}

	@RequestMapping("/editformform")
	public String showEditPage() {
		return "editform";
	}

	@RequestMapping("/deleteform")
	public String showDeletePage() {
		return "deleteform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(ModelMap map, @RequestParam String sid, @RequestParam String sname, @RequestParam String saddr) {
		map.addAttribute("operation", "Student Insertion");
		Student std = new Student();
		std.setSid(sid);
		std.setSname(sname);
		std.setSaddr(saddr);
		String status = studentService.addStudent(std);
		return status;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String search(ModelMap map, @RequestParam String sid) {
		String status = "";
		Student std = studentService.searchStudent(sid);
		if (std == null) {
			status = "notexisted";
		} else {
			map.addAttribute("std", std);
			status = "studentdetails";
		}
		return status;
	}

	@RequestMapping(value = "/editform", method = RequestMethod.POST)
	public String getEditForm(ModelMap map, @RequestParam String sid) {
		String status = "";
		Student std = studentService.searchStudent(sid);
		if (std == null) {
			status = "notexisted";
		} else {
			map.addAttribute("std", std);
			status = "editform";
		}
		return status;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(ModelMap map, @RequestParam String sid, @RequestParam String sname,
			@RequestParam String saddr) {
		String status = "";
		map.addAttribute("operation", "Student Updation");
		Student std = new Student();
		std.setSid(sid);
		std.setSname(sname);
		std.setSaddr(saddr);
		status = studentService.updateStudent(std);
		return status;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(ModelMap map, @RequestParam String sid) {
		map.addAttribute("operation", "Student Deletion");
		String status = studentService.deleteStudent(sid);
		return status;
	}

}
